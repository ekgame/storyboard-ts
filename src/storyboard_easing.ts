import {Easing} from "./storyboard_units";

/**
 * Apply easing to a progress.
 * Mostly based on tween.js
 * @param {Easing} type Easing type.
 * @param {Number} x The current progress from 0 to 1.
 * @returns {Number} The eased progress from 0 to 1.
 */
export function apply(type: Easing, x: number): number {
    if (x === 0) return 0;
    if (x === 1) return 1;

    function powIn(power: number) {
        return Math.pow(x, power);
    }

    function powOut(power: number) {
        return 1 - Math.pow(1 - x, power);
    }

    function powInOut(power: number) {
        if ((x *= 2) < 1)
            return 0.5*Math.pow(x, power);
        return 1-0.5*Math.abs(Math.pow(2 - x, power));
    }

    switch (type) {
        case Easing.LINEAR: return x;

        case Easing.EASE_IN:
        case Easing.QUAD_IN:      return powIn(2);
        case Easing.EASE_OUT:
        case Easing.QUAD_OUT:     return powOut(2);
        case Easing.QUAD_IN_OUT:  return powInOut(2);

        case Easing.CUBIC_IN:     return powIn(3);
        case Easing.CUBIC_OUT:    return powOut(3);
        case Easing.CUBIC_IN_OUT: return powInOut(3);

        case Easing.QUART_IN:     return powIn(4);
        case Easing.QUART_OUT:    return powOut(4);
        case Easing.QUART_IN_OUT: return powInOut(4);

        case Easing.QUINT_IN:     return powIn(5);
        case Easing.QUINT_OUT:    return powOut(5);
        case Easing.QUINT_IN_OUT: return powInOut(5);

        case Easing.SINE_IN:      return 1 - Math.cos(x*Math.PI/2);
        case Easing.SINE_OUT:     return Math.sin(x*Math.PI/2);
        case Easing.SINE_IN_OUT:  return -0.5*(Math.cos(Math.PI*x) - 1);

        case Easing.EXPO_IN:      return Math.pow(1024, x - 1);
        case Easing.EXPO_OUT:     return 1 - Math.pow(2, - 10 * x);
        case Easing.EXPO_IN_OUT:
            if ((x *= 2) < 1)
                return 0.5 * Math.pow(1024, x - 1);
            return 0.5 * (-Math.pow(2, -10 * (x - 1)) + 2);

        case Easing.CIRC_IN:  return 1 - Math.sqrt(1 - x*x);
        case Easing.CIRC_OUT: return Math.sqrt(1 - (--x*x));
        case Easing.CIRC_IN_OUT:
            if ((x *= 2) < 1)
                return - 0.5*(Math.sqrt(1 - x*x) - 1);
            return 0.5*(Math.sqrt(1-(x -= 2)*x) + 1);

        case Easing.ELASTIC_IN:
            return -Math.pow(2, 10 * (x - 1)) * Math.sin((x - 1.1)*5*Math.PI);
        case Easing.ELASTIC_OUT:
            return Math.pow(2, -10 * x) * Math.sin((x - 0.1)*5*Math.PI) + 1;
        case Easing.ELASTIC_HALF_OUT:
            return Math.pow(2, -10 * x) * Math.sin(0.5*(x - 0.1)*5*Math.PI) + 1;
        case Easing.ELASTIC_QUARTER_OUT:
            return Math.pow(2, -10 * x) * Math.sin(0.25*(x - 0.1)*5*Math.PI) + 1;
        case Easing.ELASTIC_IN_OUT:
            if ((x *= 2) < 1)
                return -0.5 * Math.pow(2, 10 * (x - 1)) * Math.sin((x - 1.1) * 5 * Math.PI);
            return 0.5 * Math.pow(2, -10 * (x - 1)) * Math.sin((x - 1.1) * 5 * Math.PI) + 1;

        case Easing.BACK_IN:
            var s = 1.70158;
            return x * x * ((s + 1) * x - s);
        case Easing.BACK_OUT:
            var s = 1.70158;
            return --x * x * ((s + 1) * x + s) + 1;
        case Easing.BACK_IN_OUT:
            var s = 1.70158 * 1.525;
            if ((x *= 2) < 1)
                return 0.5 * (x * x * ((s + 1) * x - s));
            return 0.5 * ((x -= 2) * x * ((s + 1) * x + s) + 2);

        case Easing.BOUNCE_IN:
            return x - apply(Easing.BOUNCE_OUT, 1 - x)
        case Easing.BOUNCE_OUT:
            var s = 7.5625;
            if (x < (1 / 2.75))
                return s*x*x;
            else if (x < (2 / 2.75))
                return s*(x -= (1.5 / 2.75))*x + 0.75;
            else if (x < (2.5 / 2.75))
                return s*(x -= (2.25 / 2.75))*x + 0.9375;
            else
                return s*(x -= (2.625 / 2.75))*x + 0.984375;
        case Easing.BOUNCE_IN_OUT:
            if (x < 0.5)
                return apply(Easing.BOUNCE_IN, x*2)*0.5;
            return apply(Easing.BOUNCE_OUT, x*2 - 1)*0.5 + 0.5;
    }
}