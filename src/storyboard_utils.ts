export function strStartsWith(_str: string, searchString: string, position?: number) {
    position = position || 0;
    return _str.substr(position, searchString.length) === searchString;
}

export function strEndsWith(_str: string, searchString: string, position?: number) {
    var subjectString = _str.toString();
    if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
    }
    position -= searchString.length;
    var lastIndex = subjectString.lastIndexOf(searchString, position);
    return lastIndex !== -1 && lastIndex === position;
}


export function strRepeat(str: string, count: number) {
    if (this == null)
        throw new TypeError('can\'t convert ' + this + ' to object');
    var str = '' + str;
    count = +count;
    if (count != count)
        count = 0;
    if (count < 0)
        throw new RangeError('repeat count must be non-negative');
    if (count == Infinity)
        throw new RangeError('repeat count must be less than infinity');
    count = Math.floor(count);
    if (str.length == 0 || count == 0)
        return '';
    if (str.length * count >= 1 << 28)
        throw new RangeError('repeat count must not overflow maximum string size');
    var rpt = '';
    for (;;) {
        if ((count & 1) == 1)
            rpt += str;
        count >>>= 1;
        if (count == 0)
            break;
        str += str;
    }
    return rpt;
}


export function mergeArgs(args: Array<string>): Array<string> {
    var iterator = new Iterator(args);
    var result: string[] = [];

    var buffer = "";
    var connecting = false;
    while (iterator.hasNext()) {
        var arg = iterator.next();
        if (strStartsWith(arg, "\"") && strEndsWith(arg, "\"")) {
            result.push(arg.substr(1, arg.length - 2));
        } else if (strStartsWith(arg, "\"") && !connecting) {
            buffer = arg.substr(1, arg.length - 1);
            connecting = true;
        }
        else if (strEndsWith(arg, "\"") && connecting) {
            result.push(buffer + "," + arg.substr(0, arg.length - 1));
            connecting = false;
        }
        else if (connecting) {
            buffer += "," + arg;
        } else {
            result.push(arg);
        }
    }
    return result;
}

export class Iterator<T> {

    private list: T[];
    private currentIndex = 0;

    constructor(list: T[]) {
        this.list = list;
    }

    hasNext(): boolean {
        return this.currentIndex < this.list.length;
    }

    hasPrevious(): boolean {
        return this.currentIndex > 0;
    }

    next(): T  {
        return this.list[this.currentIndex++];
    }

    previous(): T {
        return this.list[--this.currentIndex];
    }
}

export class ListIterator<T> {

    private list: T[];
    private currentIndex: number;
    private size: number;

    constructor(list: T[], startIndex: number, size: number) {
        this.list = list;
        this.currentIndex = startIndex;
        this.size = size;
    }

    public hasNext(): boolean {
        return (this.list.length - this.currentIndex) >= this.size;
    }

    public next(): T[] {
        var result: T[] = [];
        for (var i = 0; i < this.size; i++)
            result.push(this.list[this.currentIndex + i]);
        this.currentIndex += this.size;
        return result;
    }
}