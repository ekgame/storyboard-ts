import { Layer, Origin, LoopType } from "./storyboard_units";
import { Event } from "./storyboard_events";

export class Storyboard {

    objects: Object[] = [];

    add(object: Object) {
        this.objects.push(object);
    }

    toOSB(): string {
        var result = "[Events]\n// generated with storyboard.ts";
        for (var object of this.objects)
            result += "\n" + object.toOSB();
        return result;
    }
}

export abstract class Object {

    filePath: string;
    layer: Layer;

    constructor(filePath: string, layer?: Layer) {
        this.filePath = filePath;
        this.layer = layer || Layer.BACKGROUND;
    }

    abstract toOSB(): string;
}

export class Sprite extends Object {

    origin: Origin;
    layer: Layer;
    events: Event[] = [];

    constructor(filePath: string, origin?: Origin, layer?: Layer) {
        super(filePath, layer);
        this.origin = origin || Origin.TOP_LEFT;
        this.layer = layer || Layer.BACKGROUND;
    }

    addEvent(event: Event) {
        if (this.events.indexOf(event) === -1)
            this.events.push(event);
    }

    toOSB(): string {
        var result = `Sprite,"${this.layer.value}","${this.origin.value},"${this.filePath}",0,0`;
        for (var event of this.events) {
            var lines = event.toOSB().split("\n");
            for (var i = 0; i < lines.length; i++)
                result += "\n " + lines[i];
        }
        return result;
    }
}

export class Animation extends Sprite {

    frameCount: number;
    frameDelay: number;
    loopType: LoopType;

    constructor(filePath: string, frameCount: number, frameDelay: number, loopType: LoopType, origin?: Origin, layer?: Layer) {
        super(filePath, origin, layer);
        this.frameCount = frameCount;
        this.frameDelay = frameDelay;
        this.loopType = loopType;
    }

    toOSB(): string {
        var result = `Animation,"${this.layer.value}","${this.origin.value},"${this.filePath}",0,0,${this.frameCount},${this.frameDelay},${this.loopType.value}`;
        for (var event of this.events) {
            var lines = event.toOSB().split("\n");
            for (var i = 0; i < lines.length; i++)
                result += "\n " + lines[i];
        }
        return result + "\n";
    }
}


