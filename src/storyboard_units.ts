export enum Easing {
    LINEAR = 0,

    EASE_OUT = 1,
    EASE_IN = 2,

    QUAD_IN = 3,
    QUAD_OUT = 4,
    QUAD_IN_OUT = 5,

    CUBIC_IN = 6,
    CUBIC_OUT = 7,
    CUBIC_IN_OUT = 8,

    QUART_IN = 9,
    QUART_OUT = 10,
    QUART_IN_OUT = 11,

    QUINT_IN = 12,
    QUINT_OUT = 13,
    QUINT_IN_OUT = 14,

    SINE_IN = 15,
    SINE_OUT = 16,
    SINE_IN_OUT = 17,

    EXPO_IN = 18,
    EXPO_OUT = 19,
    EXPO_IN_OUT = 20,

    CIRC_IN = 21,
    CIRC_OUT = 22,
    CIRC_IN_OUT = 23,

    ELASTIC_IN = 24,
    ELASTIC_OUT = 25,
    ELASTIC_HALF_OUT = 26,
    ELASTIC_QUARTER_OUT = 27,
    ELASTIC_IN_OUT = 28,

    BACK_IN = 29,
    BACK_OUT = 30,
    BACK_IN_OUT = 31,

    BOUNCE_IN = 32,
    BOUNCE_OUT = 33,
    BOUNCE_IN_OUT = 34
}

export class Vec2 {

    public x: number;
    public y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}

export enum Axis {
    X, Y
}

export class Trigger {

    public readonly value: String;

    private constructor(value: String) {
        this.value = value;
    }

    static readonly CLAP = new Trigger("HitSoundClap");
    static readonly FINISH = new Trigger("HitSoundFinish");
    static readonly WHISTLE = new Trigger("HitSoundWhistle");
    static readonly PASSING = new Trigger("Passing");
    static readonly FAILING = new Trigger("Failing");

    static values(): Array<Trigger> {
        return [
            Trigger.CLAP,
            Trigger.FINISH,
            Trigger.WHISTLE,
            Trigger.PASSING,
            Trigger.FAILING,
        ]
    }

    static fromString(str: string): Trigger {
        for (var effect of this.values())
            if (effect.value === str)
                return effect;
        return Trigger.CLAP;
    }
}

export class LoopType {

    public readonly value: String;

    private constructor(value: String) {
        this.value = value;
    }

    static readonly ONCE = new LoopType("LoopOnce");
    static readonly FOREVER = new LoopType("LoopForever");

    static values(): Array<Effect> {
        return [
            LoopType.ONCE,
            LoopType.FOREVER,
        ]
    }

    static fromString(str: string): LoopType {
        for (var effect of this.values())
            if (effect.value === str)
                return effect;
        return LoopType.ONCE;
    }
}

export class Effect {

    public readonly value: String;

    private constructor(value: String) {
        this.value = value;
    }

    static readonly FLIP_HORIZONTAL = new Effect("H");
    static readonly FLIP_VERTICAL = new Effect("V");
    static readonly ADDITIVE = new Effect("A");

    static values(): Array<Effect> {
        return [
            Effect.FLIP_HORIZONTAL,
            Effect.FLIP_VERTICAL,
            Effect.ADDITIVE,
        ]
    }

    static fromString(str: string): Effect {
        for (var effect of this.values())
            if (effect.value === str)
                return effect;
        return Effect.FLIP_HORIZONTAL;
    }
}

export class Origin {

    public readonly value: String;

    private constructor(value: String) {
        this.value = value;
    }

    static readonly TOP_LEFT = new Origin("TopLeft");
    static readonly TOP_CENTER    = new Origin("TopCentre");
    static readonly TOP_RIGHT     = new Origin("TopRight");
    static readonly CENTER_LEFT   = new Origin("CentreLeft");
    static readonly CENTER        = new Origin("Centre");
    static readonly CENTER_RIGHT  = new Origin("CentreRight");
    static readonly BOTTOM_LEFT   = new Origin("BottomLeft");
    static readonly BOTTOM_CENTER = new Origin("BottomCentre");
    static readonly BOTTOM_RIGHT  = new Origin("BottomRight");

    static values(): Array<Origin> {
        return [
            Origin.TOP_LEFT, Origin.TOP_CENTER, Origin.TOP_RIGHT,
            Origin.CENTER_LEFT, Origin.CENTER, Origin.CENTER_RIGHT,
            Origin.BOTTOM_LEFT, Origin.BOTTOM_CENTER, Origin.BOTTOM_RIGHT,
        ]
    }

    static fromString(str: string): Origin {
        for (var origin of this.values())
            if (origin.value === str)
                return origin;
        return Origin.TOP_LEFT;
    }
}

export class Layer {

    public readonly value: String;

    private constructor(value: String) {
        this.value = value;
    }

    static readonly BACKGROUND = new Layer("Background");
    static readonly FOREGROUND = new Layer("Foreground");
    static readonly PASS = new Layer("Pass");
    static readonly FAIL = new Layer("Fail");

    static values(): Array<Layer> {
        return [
            Layer.BACKGROUND,
            Layer.FOREGROUND,
            Layer.PASS,
            Layer.FAIL,
        ]
    }

    static fromString(str: string) {
        for (var layer of this.values())
            if (layer.value === str)
                return layer;
        return Layer.BACKGROUND;
    }
}

export class Color {
    red: number;
    green: number;
    blue: number;

    constructor(redOrHex: number, green?: number, blue?: number) {
        if (green && blue) {
            this.red = redOrHex;
            this.green = green;
            this.blue = blue;
        }
        else {
            this.red = (redOrHex & 0xFF0000) >> 16;
            this.green = (redOrHex & 0xFF00) >> 8;
            this.blue = redOrHex & 0xFF;
        }
    }
}
