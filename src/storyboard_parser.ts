import { Storyboard, Sprite, Animation } from "./storyboard";
import {Layer, Origin, LoopType} from "./storyboard_units";
import utils = require("./storyboard_utils");
import {
    Event, EventMoveAxis, EventMove, EventFade, EventScale,
    EventVectorScale, EventRotate, EventColor, EventEffect,
    EventLoop, EventTrigger,
} from "./storyboard_events";

export class StoryboardParser {

    static parse(rawStoryboard: string): Storyboard {
        return new StoryboardParser().parseStoryboard(rawStoryboard);
    }

    private parseStoryboard(rawStoryboard: string): Storyboard {
        var storyboard = new Storyboard();
        var lines = rawStoryboard.split("\n");
        var started = false;

        var iterator = new utils.Iterator(lines);
        while (iterator.hasNext()) {
            var line = iterator.next();
            var trimmed = line.trim();

            // Skip empty lines and comments
            if (trimmed.length === 0 || trimmed.lastIndexOf("//", 0) === 0)
                continue;

            // Find where the storyboard code starts
            if (trimmed === "[Events]") {
                started = true;
                continue;
            }

            // Skip lines until start of the code is found
            if (!started)
                continue;

            if (utils.strStartsWith(line, "Sprite")) {
                storyboard.add(this.parseSprite(iterator, line));
            }
            else if (utils.strStartsWith(line, "Animation")) {
                storyboard.add(this.parseAnimation(iterator, line));
            }
        }
        return storyboard;
    }

    private parseEvents(iterator: utils.Iterator<string>, indent: number): Event[] {
        var indent1 = utils.strRepeat(" ", indent);
        var indent2 = utils.strRepeat("_", indent);
        var result: Event[] = [];

        function checkEvent(str: string, event: string): boolean {
            return utils.strStartsWith(str, indent1 + event) || utils.strStartsWith(str, indent2 + event);
        }

        while (iterator.hasNext()) {
            var line = iterator.next();
            var trimmed = line.trim();

            if (trimmed.length == 0 || utils.strStartsWith(trimmed, "//"))
                continue;

            if (checkEvent(line, "MX") || checkEvent(line, "MY"))
                result.push(EventMoveAxis.fromOSB(line));
            else if (checkEvent(line, "M"))
                result.push(EventMove.fromOSB(line));
            else if (checkEvent(line, "F"))
                result.push(EventFade.fromOSB(line));
            else if (checkEvent(line, "S"))
                result.push(EventScale.fromOSB(line));
            else if (checkEvent(line, "V"))
                result.push(EventVectorScale.fromOSB(line));
            else if (checkEvent(line, "R"))
                result.push(EventRotate.fromOSB(line));
            else if (checkEvent(line, "C"))
                result.push(EventColor.fromOSB(line));
            else if (checkEvent(line, "P"))
                result.push(EventEffect.fromOSB(line));
            else if (checkEvent(line, "L"))
                result.push(EventLoop.fromOSB(line, this.parseEvents(iterator, indent + 1)));
            else if (checkEvent(line, "T"))
                result.push(EventTrigger.fromOSB(line, this.parseEvents(iterator, indent + 1)));
            else {
                iterator.previous();
                break;
            }
        }
        return result;
    }

    private parseSprite(iterator: utils.Iterator<string>, rawObject: string): Sprite {
        var args = utils.mergeArgs(rawObject.split(","));
        var layer = Layer.fromString(args[1]);
        var origin = Origin.fromString(args[2]);
        var filePath = args[3];
        var object = new Sprite(filePath, origin, layer);
        for (var event of this.parseEvents(iterator, 1))
            object.addEvent(event);
        return object;
    }

    private parseAnimation(iterator: utils.Iterator<string>, rawObject: string): Animation {
        var args = utils.mergeArgs(rawObject.split(","));
        var layer = Layer.fromString(args[1]);
        var origin = Origin.fromString(args[2]);
        var filePath = args[3];
        var frameCount = parseInt(args[6]);
        var frameDelay = parseInt(args[7]);
        var loopType = LoopType.fromString(args[8]);

        var object = new Animation(filePath, frameCount, frameDelay, loopType, origin, layer);
        for (var event of this.parseEvents(iterator, 1))
            object.addEvent(event);
        return object;
    }
}

