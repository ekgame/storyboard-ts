import {
    Easing, Vec2, Color, Axis, Effect,
    Layer, Origin, Trigger, LoopType
} from "./storyboard_units";
import utils = require("./storyboard_utils");

/**
 * Base object for all events.
 */
export abstract class Event {

    startTime: number;
    endTime: number;
    easing: Easing;

    constructor(startTime: number, endTime: number, easing?: Easing) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.easing = easing || Easing.LINEAR;
    }

    public getDurationUnit(): number {
        return this.endTime - this.startTime;
    }

    abstract getDuration(): number;

    protected getOSBHead(type: string): string {
        if (this.startTime === this.endTime)
            return `${type},${this.easing},${this.startTime},`;
        return `${type},${this.easing},${this.startTime},${this.endTime}`;
    }

    abstract toOSB(): String;
}

/**
 * Base object for all events with parameters.
 */
export abstract class SpriteEvent<T> extends Event {

    parameters: Array<T> = [];

    constructor(startTime: number, endTime: number, easing?: Easing) {
        super(startTime, endTime, easing);
    }

    public getDuration(): number {
        return this.getDurationUnit()*this.parameters.length;
    }

    public to(value: T) {
        this.parameters.push(value);
    }
}

/**
 * Controls visibility. 0 is invisible, 1 is fully visible.
 */
export class EventFade extends SpriteEvent<number> {

    constructor(startTime: number, endTime: number, easing?: Easing, startOpacity?: number) {
        super(startTime, endTime, easing);
        if (startOpacity) this.to(startOpacity);
    }

    toOSB(): String {
        var result = this.getOSBHead("F");
        for (var opacity of this.parameters)
            result += `,${opacity}`;
        return result;
    }

    static fromOSB(line: string): EventScale {
        var args = line.split(",");
        var easing = parseInt(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventFade(startTime, endTime, easing);

        var iterator = new utils.ListIterator(args, 4, 1);
        while (iterator.hasNext())
            event.to(parseFloat(iterator.next()[0]));
        return event;
    }
}

/**
 * Controls position.
 */
export class EventMove extends SpriteEvent<Vec2> {

    constructor(startTime: number, endTime: number, easing?: Easing, startPos?: Vec2) {
        super(startTime, endTime, easing);
        if (startPos) this.to(startPos);
    }

    toOSB(): String {
        var result = this.getOSBHead("M");
        for (var pos of this.parameters)
            result += `,${pos.x},${pos.y}`;
        return result;
    }

    static fromOSB(line: string): EventMove {
        var args = line.split(",");
        var easing = parseInt(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventMove(startTime, endTime, easing);

        var iterator = new utils.ListIterator(args, 4, 2);
        while (iterator.hasNext()) {
            var rawVec = iterator.next();
            var vec = new Vec2(
                parseFloat(rawVec[0]),
                parseFloat(rawVec[1])
            );
            event.to(vec);
        }
        return event;
    }
}

/**
 * Controls scale.
 */
export class EventScale extends SpriteEvent<number> {

    constructor(startTime: number, endTime: number, easing?: Easing, startScale?: number) {
        super(startTime, endTime, easing);
        if (startScale) this.to(startScale);
    }

    toOSB(): String {
        var result = this.getOSBHead("S");
        for (var scale of this.parameters)
            result += `,${scale}`;
        return result;
    }

    static fromOSB(line: string): EventScale {
        var args = line.split(",");
        var easing = parseInt(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventScale(startTime, endTime, easing);

        var iterator = new utils.ListIterator(args, 4, 1);
        while (iterator.hasNext())
            event.to(parseFloat(iterator.next()[0]));
        return event;
    }
}

/**
 * Controls scale of different axis separately.
 */
export class EventVectorScale extends SpriteEvent<Vec2> {

    constructor(startTime: number, endTime: number, easing?: Easing, startScale?: Vec2) {
        super(startTime, endTime, easing);
        if (startScale) this.to(startScale);
    }

    toOSB(): String {
        var result = this.getOSBHead("V");
        for (var vec of this.parameters)
            result += `,${vec.x},${vec.y}`;
        return result;
    }

    static fromOSB(line: string): EventVectorScale {
        var args = line.split(",");
        var easing = parseInt(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventVectorScale(startTime, endTime, easing);

        var iterator = new utils.ListIterator(args, 4, 2);
        while (iterator.hasNext()) {
            var rawVec = iterator.next();
            var vec = new Vec2(
                parseFloat(rawVec[0]),
                parseFloat(rawVec[1])
            );
            event.to(vec);
        }
        return event;
    }
}

/**
 * Controls rotation. The rotation is defined in radians.
 * radians = degrees*(pi/180)
 */
export class EventRotate extends SpriteEvent<number> {

    constructor(startTime: number, endTime: number, easing?: Easing, startRadians?: number) {
        super(startTime, endTime, easing);
        if (startRadians) this.to(startRadians);
    }

    toOSB(): String {
        var result = this.getOSBHead("R");
        for (var radians of this.parameters)
            result += `,${radians}`;
        return result;
    }

    static fromOSB(line: string): EventRotate {
        var args = line.split(",");
        var easing = parseInt(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventRotate(startTime, endTime, easing);

        var iterator = new utils.ListIterator(args, 4, 1);
        while (iterator.hasNext())
            event.to(parseFloat(iterator.next()[0]));
        return event;
    }
}

/**
 * Controls color of the sprite.
 */
export class EventColor extends SpriteEvent<Color> {

    constructor(startTime: number, endTime: number, easing?: Easing, startColor?: Color) {
        super(startTime, endTime, easing);
        if (startColor) this.to(startColor);
    }

    toOSB(): String {
        var result = this.getOSBHead("C");
        for (var color of this.parameters)
            result += `,${color.red},${color.green},${color.blue}`;
        return result;
    }

    static fromOSB(line: string): EventColor {
        var args = line.split(",");
        var easing = parseInt(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventColor(startTime, endTime, easing);

        var iterator = new utils.ListIterator(args, 4, 3);
        while (iterator.hasNext()) {
            var rawColor = iterator.next();
            var color = new Color(
                parseInt(rawColor[0]),
                parseInt(rawColor[1]),
                parseInt(rawColor[2])
            );
            event.to(color);
        }
        return event;
    }
}

/**
 * Controls position on a single axis.
 */
export class EventMoveAxis extends SpriteEvent<number> {

    axis: Axis;

    constructor(startTime: number, endTime: number, axis: Axis, easing?: Easing, startPos?: number) {
        super(startTime, endTime, easing);
        this.axis = axis;
        if (startPos) this.to(startPos);
    }

    toOSB(): String {
        var result = this.getOSBHead("M" + Axis[this.axis]);
        for (var position of this.parameters)
            result += `,${position}`;
        return result;
    }

    static fromOSB(line: string): EventMoveAxis {
        var args = line.split(",");
        var axis = utils.strEndsWith(args[0].trim(), "X") ? Axis.X : Axis.Y;
        var easing = parseInt(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventMoveAxis(startTime, endTime, axis, easing);

        var iterator = new utils.ListIterator(args, 4, 1);
        while (iterator.hasNext())
            event.to(parseFloat(iterator.next()[0]));
        return event;
    }
}

/**
 * Allows to add basic effects to a sprite.
 */
export class EventEffect extends Event {

    effect: Effect;

    constructor(startTime: number, endTime: number, effect: Effect) {
        super(startTime, endTime);
        this.effect = effect;
    }

    getDuration(): number {
        return this.endTime - this.startTime;
    }

    toOSB(): String {
        return this.getOSBHead("P") + `,${this.effect.value}`;
    }

    static fromOSB(line: string): EventEffect {
        var args = line.split(",");
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var effect = Effect.fromString(args[4]);
        return new EventEffect(startTime, endTime, effect);
    }
}

export abstract class EventContainer extends Event {

    events: Event[] = [];

    constructor(startTime: number) {
        super(startTime, startTime);
    }

    addEvent(event: Event) {
        this.events.push(event);
    }

    abstract getDuration(): number;

    abstract toOSB(): String;
}

/**
 * Loop some event. The encompassed events' timestamp should start be 0 based.
 */
export class EventLoop extends EventContainer {

    loopCount: number;

    constructor(startTime: number, loopCount: number) {
        super(startTime);
        this.loopCount = loopCount;
    }

    getDuration(): number {
        return this.endTime - this.startTime;
    }

    addEvent(event: Event) {
        super.addEvent(event);
        var endTime = event.endTime*this.loopCount;
        if (endTime > this.endTime)
            this.endTime = endTime;
    }

    toOSB(): string {
        var result = `L,${this.startTime},${this.loopCount}`;
        for (var event of this.events)
            result += "\n " + event.toOSB();
        return result;
    }

    static fromOSB(line: string, events: Event[]): EventLoop {
        var args = line.split(",");
        var startTime = parseInt(args[1]);
        var loopCount = parseInt(args[2]);
        var event = new EventLoop(startTime, loopCount);
        for (var e of events)
            event.addEvent(e);
        return event;
    }
}

/**
 * Do something on a trigger
 */
export class EventTrigger extends EventContainer {

    loopType: LoopType;
    endTime: number;

    constructor(loopType: LoopType, startTime: number, endTime: number) {
        super(startTime);
        this.loopType = loopType;
        this.endTime = endTime;
    }

    getDuration(): number {
        var max = 0;
        for (var event of this.events)
            if (event.endTime > max)
                max = event.endTime;
        return max;
    }

    toOSB(): string {
        var result = `T,${this.loopType.value},${this.startTime},${this.endTime}`;
        for (var event of this.events)
            result += "\n " + event.toOSB();
        return result;
    }

    static fromOSB(line: string, events: Event[]): EventTrigger {
        var args = line.split(",");
        var loopType = LoopType.fromString(args[1]);
        var startTime = parseInt(args[2]);
        var endTime = parseEndTime(args[3], startTime);
        var event = new EventTrigger(loopType, startTime, endTime);
        for (var e of events)
            event.addEvent(e);
        return event;
    }
}

function parseEndTime(str: string, startTime: number): number {
    if (str === "")
        return startTime;
    return parseInt(str);
}